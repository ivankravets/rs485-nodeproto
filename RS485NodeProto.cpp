/*
 * RS485NodeProto.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#include <Arduino.h>
#include <util/crc16.h>

#include "RS485NodeProto.h"

// Byte values used for special stuff
#define STARTBYTE   0xA0
#define ESCBYTE     0xE0
#define STOPBYTE    0x80
#define ESCMASK		0xFF

// Defines number of milliseconds before returning to idle, after no bytes received.
#define TIMEOUT_IDLE   		300

// Defines minimum timeout from stop byte received until TX can be done at the earliest.
#define TIMEOUT_STOP_TX		10


uint16_t RS485NodeProto::_sendEscByteCRC(uint16_t currcrc, uint8_t db, bool noCrc) {
	// Update CRC
	if (!noCrc) currcrc = _crc_ccitt_update(currcrc, db);

	// Check for escaping
	switch (db) {
		case STARTBYTE:
		case ESCBYTE:
		case STOPBYTE:
			// Send escape byte and modify data
			_rsctrl.stream.write(ESCBYTE);
			db ^= ESCMASK;
			break;
	}
	// Write the data byte
	_rsctrl.stream.write(db);

	return currcrc;
}

void RS485NodeProto::begin() {
	_rsctrl.begin();
}

void RS485NodeProto::end() {
	_rsctrl.begin();
}

bool RS485NodeProto::sendPacket(RS485PktType pktType, uint8_t address, char data[], uint8_t length, uint8_t retry) {
	uint8_t txByte;
	// TODO: We can not transmit if RX is in progress. This needs handling!

	// Sends packet to specified node address
	retry++;

	while (retry > 0) {
		// Enable TX mode, with loop back
		bool useLoopback = _rsctrl.setTransmit(!_noColDet);

		// Send START byte
		_rsctrl.stream.write(STARTBYTE);

		// Check if loopback is enabled
		if (useLoopback) {
			_rsctrl.stream.flush();

			// Check if we receive the same byte as we sent
			if (_rsctrl.stream.available()) {
				txByte = _rsctrl.stream.read();
				if (txByte != STARTBYTE) {
					// We have a collision. Wait and retry.
					_rsctrl.endTransmit();

					// TODO: Implement wait

					retry--;
					continue;
				}
			}
			// Collision check is done, continue transmit without loopback
			_rsctrl.setTransmit(false);
		}

		// Prepare the Destionation header byte
		txByte = pktType | (address & 0x3F);
		uint16_t txcrc = _sendEscByteCRC(0, txByte);

		// Prepare the Source Header byte
		txByte = (_nodeId & 0x3F);
		txcrc = _sendEscByteCRC(txcrc, txByte);

		// Send the packet data...
		for (uint8_t i = 0; i < length; i++) {
			txcrc = _sendEscByteCRC(txcrc, data[i]);
		}

		// Send the CRC
		_sendEscByteCRC(0, txcrc >> 8,   true);
		_sendEscByteCRC(0, txcrc & 0xFF, true);

		// Send the STOP byte
		_rsctrl.stream.write(STOPBYTE);

		// End transmit
		_rsctrl.endTransmit();

		break;
	}

	return true;
}

RS485ProtoStatus RS485NodeProto::update() {
	uint8_t rcvByte;
	RS485ProtoStatus recvStatus = IDLE_OK;

	// Determine if we should check for state timeout.
	if (_rxState != RX_IDLE && !_rsctrl.stream.available()) {
		if (millis() - _rxLastByte > TIMEOUT_IDLE) {
			_rxState = RX_IDLE;
		}
	}

	// Clear out of here, if there is no data available.
	if (!_rsctrl.stream.available())  return recvStatus;

	// Loop as long as data is available to empty serial buffer
	while (true) {
		// Read the current byte
		rcvByte = _rsctrl.stream.read();
		_rxLastByte = millis();

		// Stop bytes must be handled specially, as they can happen any time (except in Escapes)
		recvStatus = BUSY;
		if (!_rxEscState && rcvByte == STOPBYTE) {
			// If we where in data receive mode
			recvStatus = IDLE_OK;
			if (_rxState != RX_RCVDATA) {
				// Go directly to STOP except if we are in RCVDATA Mode.
				_rxState = RX_STOP;
				// Continue, as there are not much more to do.
				continue;
			}
			else {
				// Change state to VERIFY
				_rxState = RX_VERIFY;
			}
		}
		else {
			// Check for escapes
			if (rcvByte == ESCBYTE && _rxState > RX_IDLE) {
				_rxEscState = true;
				continue;
			}
			if (_rxEscState) {
				rcvByte ^= ESCMASK;
				_rxEscState = false;
			}
		}

		// Determine action based on state
		switch(_rxState) {
		case RX_IDLE:
		case RX_STOP:
			// No state change if we get a stop byte.
			if (rcvByte == STOPBYTE) break;
			if (rcvByte != STARTBYTE) {
				// Go directly to IGNORE mode if not a start byte.
				_rxState = RX_IGN;
				break;
			}

			// We got a start byte, so we should continue to read DESTINATION address
			_rxState = RX_HDEST;
			break;

		case RX_HDEST:
			// Check for Single or Multicast packet
			if ((rcvByte & 0xC0) == MULTICAST) {
				// Check Multicast mask
				if (((rcvByte & 0x3F) & _nodeId) != (rcvByte & 0x3F)) {
					// Nope, mask does not match
					_rxState = RX_IGN;
					break;
				}
			}
			// Not Multicast, so check if the address is for us
			else if ((rcvByte & 0x3F) != _nodeId) {
				// Nope it's not. Ignore following data.
				_rxState = RX_IGN;
				break;
			}

			// Yup, the packet is for us. Record the packet type and clear the buffer
			recvPacket.length = 0;
			recvPacket.type = (RS485PktType)(rcvByte & 0xC0);
			_rxState = RX_HSRC;
			// Do the initial CRC calculation
			_rxCrc16 = _crc_ccitt_update(0, rcvByte);
			break;

		case RX_HSRC:
			// The packet is for us, so simply get the source address, reset length and continue.
			recvPacket.srcNodeId = rcvByte & 0x3F;
			recvPacket.length = 0;
			_rxState = RX_RCVDATA;
			_rxCrc16 = _crc_ccitt_update(_rxCrc16, rcvByte);
			break;

		case RX_RCVDATA:
			// Check for buffer overflow.
			if (recvPacket.length >= RS485_RX_BUFFERSIZE) {

				// TODO: Send NACK, in case this is a reliable packet for us.

				_rxState = RX_IGN;
				break;
			}

			recvPacket.buffer[recvPacket.length++] = rcvByte;
			if (recvPacket.length > 2) {
				_rxCrc16 = _crc_ccitt_update(_rxCrc16, recvPacket.buffer[recvPacket.length - 3]);
			}
			break;

		case RX_VERIFY:
			// Packet reception is complete. Verify CRC.
			//uint16_t rxCrcVerify = ((uint16_t)recvPacket.buffer[recvPacket.length - 2] << 8) | recvPacket.buffer[recvPacket.length - 1];

			// TODO: IMPLEMENT...
			if (true) {
				// SEND ACK!
				recvStatus = DATA_READY;
			}
			else {
				// SEND NACK!
			}
			// Change state to STOP
			_rxState = RX_STOP;
			break;

		default:
			// Catches IGNORE, which simply drops bytes
			break;
		}

		// Wait for next byte
	}

	return recvStatus;
}


