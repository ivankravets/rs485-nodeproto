/*
 * RS485NodeProto.h
 *
 * Protocol designed for RS-485, which supports Master/Slave and Multi-master
 * topologies.
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#ifndef LIB_RS485LIB_RS485NODEPROTO_H_
#define LIB_RS485LIB_RS485NODEPROTO_H_

#include "RS485Controller.h"

#define RS485_RX_BUFFERSIZE 	128

/**
 * Defines the Packet Type to send
 */
//enum RS485PktType { RELIABLE = 0x00, UNRELIABLE = 0x40, MULTICAST = 0x80, MULTICAST_SINGLE = 0xC0 };
enum RS485PktType      { SINGLE = 0x00, MULTICAST = 0x40, PKT_ACK = 0x80, PKT_NACK = 0xC0 };
enum RS485ProtoStatus  { IDLE_OK, BUSY, DATA_READY, ACK, NACK, ERROR };

/**
 * Structure used for receiving packets
 */
struct RS485Packet {
	RS485PktType type;
	uint8_t 	 srcNodeId;
	uint8_t		 length;
	uint8_t  	 buffer[RS485_RX_BUFFERSIZE];
};

class RS485NodeProto {
public:
	enum SendStatus { OK, COLLISION, TIMEOUT, NOACK };

	/**
	 * Initializes the RS485 Node Protocol
	 *
	 * @param ctrl The RS485 Controller to use for protocol handling
	 * @param nodeId The current node ID, from 0 to 63 (will be concatenated)
	 * @param retry (optional) The number of retries to execute if a MESSAGE packet is not Acknowledged. Default 3 times
	 * @param noCollitionDetect (optional) If true, no collition detection will be attempted
	 */
	RS485NodeProto(RS485Controller& ctrl, const uint8_t nodeId, const uint8_t retry = 3, const bool noCollitionDetect = false) :
		_rsctrl(ctrl), _nodeId(nodeId & 0x3F), _noColDet(noCollitionDetect)
	{
		_rxState = RX_IDLE;
		_rxEscState = false;
		_rxLastByte = 0;
		_rxCrc16 = 0;
	}

	void begin(), end();

	/**
	 * Sends a packet to the Node which has the ID specified in address.
	 *
	 * Behavior depends on the pktType parameter:
	 * - RELIABLE packets requires an ACK to be received before sendPacket returns (or retry attempts are exhausted)
	 * - UNRELIABLE will only be sent once, and sendPacket will not wait for an ACK. False is returned only on detected collisions.
	 * - MULTICAST will be retried if a collision is detected, but does not wait for an ACK
	 * - MULTICAST_UNRELIABLE will be send once, and returns false only if a collision is detected
	 *
	 * When a Multicast packet is sent, the address defines a bit-mask which defines which nodes should receive
	 * the packet. A node will receive the packet if (nodeId & bit-mask == bit-mask), and discard the packet otherwise.
	 * To send a packet to all nodes, use address 0x3F (the 3 MSBs are ignored so 0xFF work fine as well).
	 *
	 * @param pktType The packet type to send
	 * @param address The Node ID to send the packet to, or bit-mask for multicast packets
	 * @param data The data to send
	 * @param length The length of the data to send
	 */
	bool sendPacket(RS485PktType pktType, uint8_t address, char data[], uint8_t length, uint8_t retry = 0);

	/**
	 * Protocol task update. Should be called as often as possible, as all reception is handled here.
	 *
	 * When a full packet is received, the packet information can be accessed directly in the
	 * "recvPacket" member.
	 *
	 * @return True if a valid packet has been received, and false if not.
	 */
	RS485ProtoStatus update();

	/**
	 * Exposes the packet received. The data will only be valid after update() has returned
	 * "true", and only up till update is called again. Using this data except in this condition
	 * will give unpredictable results
	 */
	struct RS485Packet recvPacket;

protected:
	enum RS485RxState { RX_IDLE, RX_IGN, RX_HDEST, RX_HSRC, RX_RCVDATA, RX_VERIFY, RX_STOP };

	RS485Controller& _rsctrl;
	const uint8_t _nodeId;
	const bool _noColDet;

	RS485RxState _rxState;
	bool _rxEscState;
	unsigned long _rxLastByte;
	uint16_t 	_rxCrc16;

	/**
	 * Sends a single byte, and escapes it if necessary
	 */
	uint16_t _sendEscByteCRC(uint16_t currcrc, uint8_t db, bool noCRC = false);

};



#endif /* LIB_RS485LIB_RS485NODEPROTO_H_ */
