/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */
#include <Arduino.h>
//#include <RS485Controller.h>
#include <RS485NodeProto.h>

#include <AltSoftSerial.h>

#define LEDPIN 13

#define RS485_SOFT_RX 8
#define RS485_SOFT_TX 9

#define RS485_RE 10
#define RS485_WE 11

//#define RXNODE

// Create a soft serial, and pass to RS485 controller
AltSoftSerial softSerial;

RS485Controller rs485ctrl(softSerial, RS485_RE, RS485_WE);

#ifdef RXNODE
RS485NodeProto  rs485proto(rs485ctrl, 2);
#else
RS485NodeProto  rs485proto(rs485ctrl, 1);
#endif

int rndnum = 0;
String readstr;
uint8_t ledToggle = HIGH;

unsigned long lastmillis;

// the setup function runs once when you press reset or power the board
void setup() {
	// initialize digital pin 13 as an output.
	pinMode(LEDPIN, OUTPUT);

	Serial.begin(9600);

	softSerial.begin(9600);
	rs485proto.begin();
	softSerial.listen();

	// To get shit out of sync
	randomSeed(analogRead(0));
	rndnum = random(1000);
	delay(rndnum);

	lastmillis = millis();
}

char* TXDATA = (char*)"TESTER SENDING";

void testTransmit() {
	delay(2000);

	Serial.println("TX: Sending packet...");

	if (rs485proto.sendPacket(SINGLE, 2, TXDATA, strlen(TXDATA), 3)) {
		Serial.println("TX: OK");
	}
	else {
		Serial.println("TX: ERROR");
	}
}

RS485ProtoStatus rxstatus = ERROR;
void testRecieve() {
	// Run Update
	RS485ProtoStatus newRXstatus = rs485proto.update();

	// Print the status
	if (newRXstatus != rxstatus) {
		rxstatus = newRXstatus;

		switch(newRXstatus) {
		case IDLE_OK: 	 Serial.println("RX: IDLE"); break;
		case BUSY:		 Serial.println("RX: BUSY"); break;
		case DATA_READY: Serial.println("RX: DATA"); break;
		case ACK:		 Serial.println("RX: ACK"); break;
		case NACK:		 Serial.println("RX: NACK"); break;
		case ERROR:		 Serial.println("RX: ERROR"); break;
		}
	}
}

// the loop function runs over and over again forever
void loop() {

#ifdef RXNODE
	testRecieve();
#else
	testTransmit();
#endif

//	while (rs485ctrl.stream.available()) {
//		uint8_t rbyte = rs485ctrl.stream.read();
//		Serial.print("RX Data: ");
//		Serial.println(rbyte);
//	}
//
//	unsigned int nowMillis = millis();
//	if (nowMillis - lastmillis > 1000) {
//		digitalWrite(LEDPIN, ledToggle);
//		ledToggle = (ledToggle == HIGH ? LOW : HIGH);
//		lastmillis = nowMillis;
//
//		// Send a message when ever LED toggle is HIGH
//		if (ledToggle == HIGH) {
//			rs485ctrl.setTransmit(true);
//			rs485ctrl.stream.write((uint8_t) rndnum);
//			/*
//			rs485ctrl.stream.print("RNDVAL: ");
//			rs485ctrl.stream.println(rndnum);
//			*/
//			rs485ctrl.stream.flush();
//			rs485ctrl.endTransmit();
//			/*
//			Serial.print("TX Data: RNDVAL: ");
//			Serial.println(rndnum);*/
//		}
//	}
}
